#include "Context.h"
#include <iostream>

Context::Context()
{

}

void Context::setState(State* state)
//Used to change the current state of the state machine.
{
	if (currentGameState != NULL)
	{
		delete currentGameState;
	}

	currentGameState = state;
}

void Context::update(SDL_Renderer* renderer, bool &quit)
{
	currentGameState->update(renderer, quit);
}