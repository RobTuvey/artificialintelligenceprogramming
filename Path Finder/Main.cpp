#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include <iostream>
#include "Context.h"
#include "Menu.h"
#include "Clock.h"

int SCREEN_WIDTH = 800;
int SCREEN_HEIGHT = 600;

SDL_Window* mainWindow = NULL;
SDL_Renderer* mainRenderer = NULL;

bool Init()
{
	bool success = true;

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "SDL could not be initialized, SDL ERROR: " << SDL_GetError() << std::endl;
		success = false;
	}
	else
	{
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		{
			std::cout << "Warning! Linear texture filturing could not be initialized." << std::endl;
		}

		mainWindow = SDL_CreateWindow("Path Finder", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (mainWindow == NULL)
		{
			std::cout << "The window could not be created, SDL ERROR: " << SDL_GetError() << std::endl;
			success = false;
		}
		else
		{
			mainRenderer = SDL_CreateRenderer(mainWindow, -1, SDL_RENDERER_ACCELERATED);
			if (mainRenderer == NULL)
			{
				std::cout << "The renderer could not be created, SDL ERROR: " << SDL_GetError() << std::endl;
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor(mainRenderer, 0x00, 0x00, 0x00, 0xFF);

				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					std::cout << "SDL Image could not be initialized, SDL ERROR: " << std::endl;
					success = false;
				}

				if (TTF_Init() != 0)
				{
					std::cout << "Initializing ttf failed, TTF Error: " << TTF_GetError() << std::endl;
					success = false;
				}
			}
		}
	}

	return success;
}

void getInput(bool &quit)
{
	SDL_Event e;
	
	while (SDL_PollEvent(&e) != 0) //Polls the event queue
	{
		if (e.type == SDL_QUIT)//Checks if the close window button has been pressed.
		{
			quit = true;
		}
	}
}

int main(int argc, char* args[])
{
	if (!Init())
	{
		std::cout << "Failed to initialize!" << std::endl;
	}
	else
	{
		bool quit = false;

		Context gameState; //Sets up the state machine and initializes it with the menu state.
		gameState.setState(new Menu(&gameState, mainRenderer));

		Clock mainClock;
		mainClock.start();//initializes and starts the clock.

		while (!quit)
		{
			getInput(quit);
			
			if (mainClock.getTicks() % 34 == 0) //Checks to see if enough time has passed for an update.
			{
				SDL_RenderClear(mainRenderer);
				gameState.update(mainRenderer, quit);
				SDL_RenderPresent(mainRenderer);
			}
		}
	}

	SDL_Quit();
	IMG_Quit();
	TTF_Quit();

	return 0;
}