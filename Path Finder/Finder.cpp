#include "Finder.h"
#include "Context.h"

Tile Finder::tiles[50][50];

Finder::Finder(Context* manager, SDL_Renderer* renderer) : State(manager)
{
	//Initializes all of the buttons and sets their variables and textures.
	regenButton.setX(650);
	regenButton.setY(20);
	regenButton.setW(100);
	regenButton.setH(50);
	regenButton.setTexture(loadTexture(renderer, "Resources\\Buttons\\regenButton.png"));
	regenButton.setHighlightRing(loadTexture(renderer, "Resources\\Buttons\\highlightRing.png"));

	clearButton.setX(650);
	clearButton.setY(80);
	clearButton.setW(100);
	clearButton.setH(50);
	clearButton.setTexture(loadTexture(renderer, "Resources\\Buttons\\clearButton.png"));
	clearButton.setHighlightRing(loadTexture(renderer, "Resources\\Buttons\\highlightRing.png"));

	pathButton.setX(650);
	pathButton.setY(140);
	pathButton.setW(100);
	pathButton.setH(50);
	pathButton.setTexture(loadTexture(renderer, "Resources\\Buttons\\pathButton.png"));
	pathButton.setHighlightRing(loadTexture(renderer, "Resources\\Buttons\\highlightRing.png"));

	menuButton.setX(650);
	menuButton.setY(200);
	menuButton.setW(100);
	menuButton.setH(50);
	menuButton.setTexture(loadTexture(renderer, "Resources\\Buttons\\menuButton.png"));
	menuButton.setHighlightRing(loadTexture(renderer, "Resources\\Buttons\\highlightRing.png"));

	maxTiles = 50;//The maximum number of tiles on each axis in the grid.

	//Loads all of the textures for the tiles and other assets
	tile0 = loadTexture(renderer, "Resources\\Tiles\\tile0.png");
	tile1 = loadTexture(renderer, "Resources\\Tiles\\tile1.png");
	tile2 = loadTexture(renderer, "Resources\\Tiles\\tile2.png");
	tile3 = loadTexture(renderer, "Resources\\Tiles\\tile3.png");
	tile4 = loadTexture(renderer, "Resources\\Tiles\\tile4.png");
	tile5 = loadTexture(renderer, "Resources\\Tiles\\tile5.png");
	tile6 = loadTexture(renderer, "Resources\\Tiles\\tile6.png");
	tileHilighter = loadTexture(renderer, "Resources\\Tiles\\tileHighlight.png");
	loadingScreen = loadTexture(renderer, "Resources\\loadScreen.png");
	instructionPanel = loadTexture(renderer, "Resources\\instructions.png");

	font = TTF_OpenFont("Resources\\OpenSans.ttf", 18);

	firstBuild = false;
	loading = false;

	tile1X = 0;
	tile1Y = 0;

	tile2X = 0;
	tile2Y = 0;

	clickBuffer = 0;
}

void Finder::buildMap()
{
	srand(time(0));
	resetTiles();
	setTiles();
	loading = false;
}

void Finder::resetTiles()
//Clears all tiles and reinitializes them ready to be regenerated.
{
	int dim = 12;
	for (int x = 0; x < maxTiles; x++)
	{
		for (int y = 0; y < maxTiles; y++)
		{
			tiles[x][y].setX(x * dim);
			tiles[x][y].setY(y * dim);
			tiles[x][y].setW(dim);
			tiles[x][y].setH(dim);
			tiles[x][y].setTileX(x);
			tiles[x][y].setTileY(y);
			tiles[x][y].setType(0);
			tiles[x][y].setTexture(NULL);
			tiles[x][y].setVisited(false);
			tiles[x][y].setMerged(false);
		}
	}
}

void Finder::setTiles()
{
	numberRooms = 10;
	int placeAttempts = 0;
	int attempts = 0;
	bool placed = false;
	int newRoom_X = 0;
	int newRoom_Y = 0;
	int newRoom_W = 0;
	int newRoom_H = 0;
	for (int i = 0; i < numberRooms; i++)
	{
		placeAttempts = 0;
		attempts = 0;
		placed = false;
		newRoom_X = 0;
		newRoom_Y = 0;
		newRoom_W = 0;
		newRoom_H = 0;

		do
		{
			do //This first do loop attempts to find a place on the map that is not part of a room or a wall.
			{
				newRoom_X = (rand() % (maxTiles - 12)) + 3;
				newRoom_Y = (rand() % (maxTiles - 12)) + 3;
				attempts++;

				if (newRoom_X % 2 == 0)
				{
					if (newRoom_X >= 2) newRoom_X--;
					else newRoom_X++;
				}
				if (newRoom_Y % 2 == 0)
				{
					if (newRoom_Y >= 2) newRoom_Y--;
					else newRoom_Y++;
				}
			} while (tiles[newRoom_X][newRoom_Y].getType() != 0 && attempts < 100);
			if (attempts >= 100)
			{
				std::cout << "Failed trying to find a new room position" << std::endl;
			}
			else
			{
				attempts = 0;
				do//This attempts to find appropriate dimensions for the room.
				{
					newRoom_W = (rand() % 5) + 10;
					newRoom_H = (rand() % 5) + 10;
					attempts++;
					if (newRoom_W % 2 == 1) newRoom_W--;
					if (newRoom_H % 2 == 1) newRoom_H--;
				} while (!((newRoom_X + newRoom_W < maxTiles - 1) && (newRoom_Y + newRoom_H < maxTiles - 1)) && attempts < 100);
				if (attempts >= 100)
				{
					std::cout << "The room size could not be set." << std::endl;
				}
				else
				{
					attempts = 0;
					if (checkRoom(newRoom_X, newRoom_Y, newRoom_W, newRoom_H)) //This checks to see if the room with these dimensions and placement will overlap with any other rooms or walls.
					{
						placeRoom(newRoom_X, newRoom_Y, newRoom_W, newRoom_H);
						placed = true;
					}
					else std::cout << "Room cannot be placed in current position. X: " << newRoom_X << " Y: " << newRoom_Y << " W: " << newRoom_W << " H: " << newRoom_Y << std::endl;
				}
			}
			placeAttempts++;
		} while (!placed && placeAttempts < 5);
		if (placeAttempts >= 30) std::cout << "No valid position for the room found." << std::endl;
	}
	buildCorridors();
	setTileTextures();
}

bool Finder::checkRoom(int x, int y, int w, int h)
//Checks a room to see if it overlaps with any other rooms.
{
	for (int X = x; X < (x + w); X++)
	{
		for (int Y = y; Y < (y + h); Y++)
		{
			if (tiles[X][Y].getType() == 2)
			{
				return false;
			}
		}
	}
	return true;
}

void Finder::placeRoom(int x, int y, int w, int h)
//Once a valid room has been found this places it within the tile grid.
{
	for (int X = x; X <= (x + w); X++)
	{
		for (int Y = y; Y <= (y + h); Y++)
		{
			if ((X == x) || (X == (x + w)) || (Y == y) || (Y == (y + h)))
			{
				tiles[X][Y].setType(1);
			}
			else
			{
				tiles[X][Y].setType(2);
			}
			tiles[X][Y].setVisited(true);
		}
	}
}

void Finder::buildCorridors()
//This initializes the corridor building process, finding an appropriate place to start 'carving' the corridors.
{
	int corridorStart_X = 0;
	int corridorStart_Y = 0;
	int i = 0;
	do
	{
		corridorStart_X = rand() % maxTiles;
		corridorStart_Y = rand() % maxTiles;
		if (corridorStart_X % 2 == 1) corridorStart_X--;
		if (corridorStart_Y % 2 == 0) corridorStart_Y--;
	} while (tiles[corridorStart_X][corridorStart_Y].getType() != 0);
	carveCorridor(corridorStart_X, corridorStart_Y, i, UP);
	mergeDungeon();
}

void Finder::carveCorridor(int X, int Y, int &i, int dir)
{
	std::cout << "Maze Cell " << i << " X: " << X << " Y: " << Y << std::endl;
	i++;
	int handle = 0;

	bool result = false;

	tiles[X][Y].setType(2);
	tiles[X][Y].setVisited(true);
	tiles[X][Y].setMerged(true);

	switch (dir) //This handles logic for each of the directions the corridor can be moving in.
	{
	case UP:
		tiles[X][Y - 1].setType(2);
		tiles[X][Y - 1].setVisited(true);
		tiles[X][Y - 1].setMerged(true);
		Y -= 1;
		break;
	case DOWN:
		tiles[X][Y + 1].setType(2);
		tiles[X][Y + 1].setVisited(true);
		tiles[X][Y + 1].setMerged(true);
		Y += 1;
		break;
	case LEFT:
		tiles[X - 1][Y].setType(2);
		tiles[X - 1][Y].setVisited(true);
		tiles[X - 1][Y].setMerged(true);
		X -= 1;
		break;
	case RIGHT:
		tiles[X + 1][Y].setType(2);
		tiles[X + 1][Y].setVisited(true);
		tiles[X + 1][Y].setMerged(true);
		X += 1;
		break;
	}

	bool *up = new bool(), *down = new bool(), *left = new bool(), *right = new bool();

	do //This handles the logic for recursively carving the corridor in each direction with a weighted chance of it carrying on i the same direction it is going if it can.
	{
		switch (rand() % 20)
		{
		case UP:
			if (!*up)
			{
				if (validateMove(X, Y - 1, UP))
				{
					carveCorridor(X, Y - 1, i, UP);
				}
				*up = true;
			}
			break;
		case DOWN:
			if (!*down)
			{
				if (validateMove(X, Y + 1, DOWN))
				{
					carveCorridor(X, Y + 1, i, DOWN);
				}
				*down = true;
			}
			break;
		case LEFT:
			if (!*left)
			{
				if (validateMove(X - 1, Y, LEFT))
				{
					carveCorridor(X - 1, Y, i, LEFT);
				}
				*left = true;
			}
			break;
		case RIGHT:
			if (!*right)
			{
				if (validateMove(X + 1, Y, RIGHT))
				{
					carveCorridor(X + 1, Y, i, RIGHT);
				}
				*right = true;
			}
			break;
		default:
			switch (dir)
			{
			case UP:
				if (!*up)
				{
					if (validateMove(X, Y - 1, UP))
					{
						carveCorridor(X, Y - 1, i, UP);
					}
					*up = true;
				}
				break;
			case DOWN:
				if (!*down)
				{
					if (validateMove(X, Y + 1, DOWN))
					{
						carveCorridor(X, Y + 1, i, DOWN);
					}
					*down = true;
				}
				break;
			case LEFT:
				if (!*left)
				{
					if (validateMove(X - 1, Y, LEFT))
					{
						carveCorridor(X - 1, Y, i, LEFT);
					}
					*left = true;
				}
				break;
			case RIGHT:
				if (!*right)
				{
					if (validateMove(X + 1, Y, RIGHT))
					{
						carveCorridor(X + 1, Y, i, RIGHT);
					}
					*right = true;
				}
				break;
			}
			break;
		}
	} while (!*up || !*down || !*left || !*right);
}

bool Finder::validateMove(int X, int Y, int dir)
//This checks to see if a move the corridor is trying to make can be mad within the confides of the tile grid.
{
	bool result = true;

	switch (dir)
	{
	case UP:
		if (Y <= 2 || tiles[X][Y].getVisited() || tiles[X][Y - 1].getVisited())
		{
			return false;
		}
		break;
	case DOWN:
		if (Y >=(maxTiles - 2) || tiles[X][Y].getVisited() || tiles[X][Y + 1].getVisited())
		{
			return false;
		}
		break;
	case LEFT:
		if (X <= 2 || tiles[X][Y].getVisited() || tiles[X - 1][Y].getVisited())
		{
			return false;
		}
		break;
	case RIGHT:
		if (X >=(maxTiles - 2) || tiles[X][Y].getVisited() || tiles[X + 1][Y].getVisited())
		{
			return false;
		}
		break;
	}

	return result;
}

void Finder::mergeDungeon()
//This places the doors down on the rooms and ensures that every part of the dungeon is merged and connected someway.
{
	int i = 0;
	int handle = 0;

	int newDoor_X = 0;
	int newDoor_Y = 0;

	do
	{
		handle = 0;
		do
		{
			newDoor_X = rand() % maxTiles;
			newDoor_Y = rand() % maxTiles;
			handle++;
		} while (handle < 200 && tiles[newDoor_X][newDoor_Y].getType() != 1);
		

		if ((tiles[newDoor_X - 1][newDoor_Y].getType() == 2 && tiles[newDoor_X + 1][newDoor_Y].getType() == 2) && ((!tiles[newDoor_X - 1][newDoor_Y].getMerged() && tiles[newDoor_X + 1][newDoor_Y].getMerged()) || (!tiles[newDoor_X + 1][newDoor_Y].getMerged() && tiles[newDoor_X - 1][newDoor_Y].getMerged()) || (rand() % 40) == 0))
		{
			tiles[newDoor_X][newDoor_Y].setType(3);
			if (tiles[newDoor_X - 1][newDoor_Y].getMerged() == false)
			{
				fillMerged(newDoor_X - 1, newDoor_Y);
			}
			else if (tiles[newDoor_X + 1][newDoor_Y].getMerged() == false)
			{
				fillMerged(newDoor_X + 1, newDoor_Y);
			}
		}
		else if ((tiles[newDoor_X][newDoor_Y - 1].getType() == 2 && tiles[newDoor_X][newDoor_Y + 1].getType() == 2) && ((!tiles[newDoor_X][newDoor_Y - 1].getMerged() && tiles[newDoor_X][newDoor_Y + 1].getMerged()) || (tiles[newDoor_X][newDoor_Y - 1].getMerged() && !tiles[newDoor_X][newDoor_Y + 1].getMerged()) || (rand() % 40) == 0))
		{
			tiles[newDoor_X][newDoor_Y].setType(3);
			if (tiles[newDoor_X][newDoor_Y - 1].getMerged() == false)
			{
				fillMerged(newDoor_X, newDoor_Y - 1);
			}
			else if (tiles[newDoor_X][newDoor_Y + 1].getMerged() == false)
			{
				fillMerged(newDoor_X, newDoor_Y + 1);
			}
		}
		i++;
	} while (i != 100);
}

void Finder::fillMerged(int X, int Y) 
//This recursively fills any open space mergin it all into one.
{
	tiles[X][Y].setMerged(true);

	if (tiles[X - 1][Y].getType() == 2)
	{
		if (tiles[X - 1][Y].getMerged() == false)
		{
			fillMerged(X - 1, Y);
		}
	}
	if (tiles[X + 1][Y].getType() == 2)
	{
		if (tiles[X + 1][Y].getMerged() == false)
		{
			fillMerged(X + 1, Y);
		}
	}
	if (tiles[X][Y - 1].getType() == 2)
	{
		if (tiles[X][Y - 1].getMerged() == false)
		{
			fillMerged(X, Y - 1);
		}
	}
	if (tiles[X][Y + 1].getType() == 2)
	{
		if (tiles[X][Y + 1].getMerged() == false)
		{
			fillMerged(X, Y + 1);
		}
	}
}

void Finder::setTileTextures()
//Sets the correct textures to each tiel depending on it's type.
{
	for (int X = 0; X < maxTiles; X++)
	{
		for (int Y = 0; Y < maxTiles; Y++)
		{
			switch (tiles[X][Y].getType())
			{
			case 0:
				tiles[X][Y].setTexture(tile0);
				break;
			case 1:
				tiles[X][Y].setTexture(tile1);
				break;
			case 2:
				tiles[X][Y].setTexture(tile2);
				break;
			case 3:
				tiles[X][Y].setTexture(tile3);
				break;
			}

			tiles[X][Y].setHighlight(tileHilighter);
		}
	}
}

void Finder::clearChoice()
//Clears the tile choices.
{	
	tile1X = 0;
	tile1Y = 0;

	tile2X = 0;
	tile2Y = 0;

	chosen = false;

	setTileTextures();
}

void Finder::findPath()
//Handles the logic for controlling the pathfinding procedure.
{
	if (chosen)
	{
		Pathfinder *finder = new Pathfinder(tiles);
		finder->aStar(new Node(tile1X, tile1Y), new Node(tile2X, tile2Y));

		std::vector<Vector2*> pathToGoal = finder->getPath();

		for (int i = 0; i < pathToGoal.size() - 1; i++)
		{
			tiles[pathToGoal[i]->x][pathToGoal[i]->y].setPath(true);
			tiles[pathToGoal[i]->x][pathToGoal[i]->y].setTexture(tile4);
		}
	}
}

void Finder::update(SDL_Renderer* renderer, bool &quit)
{
	if (loading)
	{
		clearChoice();
		buildMap();
	}
	if (!firstBuild)
	{
		loading = true;
		firstBuild = true;
	}

	int mx = 0, my = 0;

	SDL_GetMouseState(&mx, &my);

	for (int X = 0; X < maxTiles; X++) //This updates all of the tiles.
	{
		for (int Y = 0; Y < maxTiles; Y++)
		{
			tiles[X][Y].update(mx, my);
		}
	}

	if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT) && clickBuffer == 0) //This just adds a buffer onto the clicking so it's easier when selecting the tiles you want to use.
	{
		clickBuffer = 1;
	}
	else if (clickBuffer > 0 && clickBuffer < 10)
	{
		clickBuffer++;
	}
	else if (clickBuffer >= 10) clickBuffer = 0;

	if (clickBuffer == 1)
	{
		for (int X = 0; X < maxTiles; X++)
		{
			for (int Y = 0; Y < maxTiles; Y++)
			{
				if (tiles[X][Y].getHover() && tiles[X][Y].getType() > 1)
				{
					if (tile1X == 0 && tile1Y == 0)
					{
						tile1X = X;
						tile1Y = Y;
						tiles[X][Y].setTexture(tile5);
					}
					else if (tile2X == 0 && tile2Y == 0)
					{
						tile2X = X;
						tile2Y = Y;
						tiles[X][Y].setTexture(tile6);
						chosen = true;
					}
				}
			}
		}
	}

	regenButton.update();
	clearButton.update();
	pathButton.update();
	menuButton.update();

	if (regenButton.getClicked()) loading = true;
	if (clearButton.getClicked()) clearChoice();
	if (pathButton.getClicked())
	{
		findPath();
	}
	if (menuButton.getClicked()) manager->setState(new Menu(manager, renderer));
	else render(renderer);
}

void Finder::render(SDL_Renderer* renderer)
{
	if (!loading)//This renders all of the tiles or the loading screen if the dungeon is being generated.
	{
		for (int X = 0; X < maxTiles; X++)
		{
			for (int Y = 0; Y < maxTiles; Y++)
			{
				tiles[X][Y].render(renderer);
			}
		}
	}
	else
	{
		SDL_Rect loadSRect = { 0, 0, 600, 600 };
		SDL_Rect loadDRect = { 7, 7, 600, 600 };
		SDL_RenderCopyEx(renderer, loadingScreen, &loadSRect, &loadDRect, 0, NULL, SDL_FLIP_NONE);
	}

	//This renders the buttons.
	regenButton.render(renderer);
	clearButton.render(renderer);
	pathButton.render(renderer);
	menuButton.render(renderer);

	//This renders the isntructions panel in the bottom right of the screen.
	SDL_Rect insSRect = { 0, 0, 150, 100 };
	SDL_Rect insDRect = { 625, 475, 150, 100 };
	SDL_RenderCopyEx(renderer, instructionPanel, &insSRect, &insDRect, 0, NULL, SDL_FLIP_NONE);

	//This renders and updates the text displaying the chosen tile information.
	SDL_Surface *til1SText;
	SDL_Surface *til2SText;
	SDL_Color fontColour = { 255, 255, 255 };
	string til1Text = "Tile 1 - X: " + std::to_string(tile1X) + " Y: " + std::to_string(tile1Y);
	string til2Text = "Tile 2 - X: " + std::to_string(tile2X) + " Y: " + std::to_string(tile2Y);
	til1SText = TTF_RenderText_Solid(font, til1Text.c_str(), fontColour);
	til2SText = TTF_RenderText_Solid(font, til2Text.c_str(), fontColour);

	SDL_Texture *til1TText = SDL_CreateTextureFromSurface(renderer, til1SText);
	SDL_Texture *til2TText = SDL_CreateTextureFromSurface(renderer, til2SText);

	SDL_Rect til1SRect = { 0, 0, til1SText->w, til1SText->h };
	SDL_Rect til1DRect = { 625, 375, til1SText->w, til1SText->h };
	SDL_RenderCopyEx(renderer, til1TText, &til1SRect, &til1DRect, 0, NULL, SDL_FLIP_NONE);

	SDL_Rect til2SRect = { 0, 0, til2SText->w, til2SText->h };
	SDL_Rect til2DRect = { 625, 425, til2SText->w, til2SText->h };
	SDL_RenderCopyEx(renderer, til2TText, &til2SRect, &til2DRect, 0, NULL, SDL_FLIP_NONE);
}