#include "Menu.h"
#include "Context.h"
Menu::Menu(Context* manager, SDL_Renderer* renderer) : State(manager)
{
	//Initializes both of the buttons and sets their variables and textures.
	startButton.setX(350);
	startButton.setY(200);
	startButton.setW(100);
	startButton.setH(50);
	startButton.setTexture(loadTexture(renderer, "Resources\\Buttons\\startButton.png"));
	startButton.setHighlightRing(loadTexture(renderer, "Resources\\Buttons\\highlightRing.png"));

	quitButton.setX(350);
	quitButton.setY(350);
	quitButton.setW(100);
	quitButton.setH(50);
	quitButton.setTexture(loadTexture(renderer, "Resources\\Buttons\\quitButton.png"));
	quitButton.setHighlightRing(loadTexture(renderer, "Resources\\Buttons\\highlightRing.png"));
}

Menu::~Menu()
{

}

void Menu::update(SDL_Renderer* renderer, bool &quit)
{
	startButton.update();
	quitButton.update();

	if (startButton.getClicked())
	{
		manager->setState(new Finder(manager, renderer));//Updates the current state in the state machine.
	}
	else if (quitButton.getClicked()) quit = true;
	else draw(renderer);
		
}

void Menu::draw(SDL_Renderer* renderer)
{
	startButton.render(renderer);
	quitButton.render(renderer);
}