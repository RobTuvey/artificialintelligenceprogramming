#include "Node.h"

Node::Node()
{
	h = 0;
	g = 0;
	f = 0;
}

Node::Node(int X, int Y)
{
	x = X;
	y = Y;
	h = 0;
	g = 0;
	f = 0;
	
}

void Node::setX(int X)
{
	x = X;
}

void Node::setY(int Y)
{
	y = Y;
}

void Node::setH(int H)
{
	h = H;
}

void Node::setG(int G)
{
	g = G;
}

void Node::setF(int F)
{
	f = F;
}

void Node::setType(int Type)
{
	type = Type;
}

void Node::setParent(Node* Parent)
{
	parent = Parent;
}

void Node::setProcessed(bool Processed)
{
	processed = Processed;
}

void Node::setClosed(bool Closed)
{
	closed = Closed;
}

void Node::calcH(Node* targetPos)
//Calculates the heuristic value using the Manhattan formula.
{
	h = (abs(x - targetPos->getX()) + abs(y - targetPos->getY()));
}