#ifndef CONTEXT_H
#define CONTEXT_H

#include "State.h"

class Context
{
private:
	State *currentGameState;

public:
	Context();
	void setState(State* state);
	void update(SDL_Renderer* renderer, bool &quit);
};

#endif