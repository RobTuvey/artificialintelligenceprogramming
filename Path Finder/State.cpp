#include "State.h"
//The state class is used to implement a state machine approach to the program to make it run as efficiently as possible.
State::State(Context* manager)
{
	this->manager = manager;
}

State::~State()
{

}

SDL_Texture* State::loadTexture(SDL_Renderer* renderer, std::string filename) 
//A function that allows me to load in a surface and coverts it into a texture
//Used by a number of classes so stored here to prevent repeats
{
	SDL_Surface* image = IMG_Load(filename.c_str());
	SDL_Texture* texture = NULL;
	if (image == NULL)
	{
		std::cout << "Image could not be loaded, SDL IMAGE ERROR: " << IMG_GetError() << std::endl;
	}
	else
	{
		texture = SDL_CreateTextureFromSurface(renderer, image);
		if (texture == NULL)
		{
			std::cout << "Texture could not be created, SDL IMAGE ERROR: " << IMG_GetError() << std::endl;
		}

		SDL_FreeSurface(image);
	}

	return texture;
}