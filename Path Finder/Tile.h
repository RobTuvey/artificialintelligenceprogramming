#ifndef TILE_H
#define TILE_H

#include "SDL.h"
#include "SDL_image.h"

class Tile
{
public:
	Tile();

	int getX() { return x; }
	int getY() { return y; }
	int getW() { return w; }
	int getH() { return h; }
	int getTileX() { return tilex; }
	int getTileY() { return tiley; }
	int getType() { return type; }
	bool getVisited() { return visited; }
	bool getMerged() { return merged; }
	bool getHover() { return hover; }
	bool getPath() { return path; }
	SDL_Texture* getTexture() { return texture; }

	void setX(int X);
	void setY(int Y);
	void setW(int W);
	void setH(int H);
	void setTileX(int TileX);
	void setTileY(int TileY);
	void setType(int Type);
	void setVisited(bool Visited);
	void setMerged(bool Merged);
	void setTexture(SDL_Texture* Texture);
	void setHighlight(SDL_Texture* Highlight);
	void setPath(bool Path);
	bool checkHover(int mx, int my);

	void update(int mx, int my);
	void render(SDL_Renderer* renderer);

private:
	int x;
	int y;
	int w;
	int h;
	int tilex;
	int tiley;
	int type;
	bool visited;
	bool merged;
	bool hover;
	bool path;
	SDL_Texture* texture;
	SDL_Texture* highlight;
};

#endif