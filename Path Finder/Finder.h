#ifndef FINDER_H
#define FINDER_H

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "Tile.h"
#include <iostream>
#include <time.h>
#include "State.h"
#include "Button.h"
#include <string>
#include "Menu.h"
#include "Pathfinder.h"
#include "Node.h"

using std::string;

enum dir
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

class Finder : public State
{
public:
	Finder(Context* manager, SDL_Renderer* renderer);

	void buildMap();
	void resetTiles();
	void setTiles();
	bool checkRoom(int x, int y, int w, int h);
	void placeRoom(int x, int y, int w, int h);
	void buildCorridors();
	void carveCorridor(int X, int Y, int &i, int dir);
	bool validateMove(int X, int Y, int dir);
	void mergeDungeon();
	void fillMerged(int X, int y);
	void setTileTextures();
	void clearChoice();
	void findPath();

	void update(SDL_Renderer* renderer, bool &quit);
	void render(SDL_Renderer* renderer);

private:
	static Tile tiles[50][50];
	int maxTiles;
	int numberRooms;
	bool firstBuild;
	bool loading;
	bool chosen;
	int clickBuffer;

	int tile1X, tile1Y;
	int tile2X, tile2Y;

	SDL_Texture* tile0;
	SDL_Texture* tile1;
	SDL_Texture* tile2;
	SDL_Texture* tile3;
	SDL_Texture* tile4;
	SDL_Texture* tile5;
	SDL_Texture* tile6;
	SDL_Texture* tileHilighter;
	SDL_Texture* loadingScreen;
	SDL_Texture* instructionPanel;

	TTF_Font *font;

	Button regenButton;
	Button clearButton;
	Button pathButton;
	Button menuButton;
};

#endif