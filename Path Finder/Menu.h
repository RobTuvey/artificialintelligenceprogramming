#ifndef MENU_H
#define MENU_H

#include "State.h"
#include "SDL.h"
#include "SDL_image.h"
#include "Button.h"
#include "Finder.h"

class Menu : public State
{
public:
	Menu(Context* manager, SDL_Renderer* renderer);
	~Menu();

	void update(SDL_Renderer* renderer, bool &quit);
	void draw(SDL_Renderer* renderer);
private:
	Button startButton;
	Button quitButton;
};

#endif