#include "Clock.h"

Clock::Clock()
{
	startTicks = 0;
	pausedTicks = 0;

	paused = false;
	started = false;
}

void Clock::start()
//Starts the clock
{
	started = true;
	paused = false;

	startTicks = SDL_GetTicks();
	pausedTicks = 0;
}

void Clock::stop()
//Stops the clock
{
	started = false;
	paused = false;

	startTicks = 0;
	pausedTicks = 0;
}

void Clock::pause()
//Pauses the clock
{
	if (started && !paused)
	{
		paused = true;
		pausedTicks = SDL_GetTicks() - startTicks;
		startTicks = 0;
	}
}

void Clock::unpause()
//Unpauses the clock
{
	if (started && paused)
	{
		paused = false;
		startTicks = SDL_GetTicks() - pausedTicks;
		pausedTicks = 0;
	}
}

Uint32 Clock::getTicks()
//Returns the number of ticks that has passed since the clock was started.
{
	Uint32 time = 0;

	if (started)
	{
		if (paused)
		{
			time = pausedTicks;
		}
		else
		{
			time = SDL_GetTicks() - startTicks;
		}
	}

	return time;
}