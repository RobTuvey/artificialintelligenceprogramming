#ifndef NODE_H
#define NODE_H

#include <cmath>

class Node
{
public:
	void setX(int X);
	void setY(int Y);
	void setH(int H);
	void setG(int G);
	void setF(int F);
	void setType(int Type);
	void setParent(Node* Parent);
	void setProcessed(bool Processed);
	void setClosed(bool Closed);

	int getX() { return x; }
	int getY() { return y; }
	int getH() { return h; }
	int getG() { return g; }
	int getF() { return f; }
	int getType(){ return type; }

	bool getProcessed(){ return processed; }
	bool getClosed(){ return closed; }

	Node* getParent() { return parent; }

	void calcH(Node* targetPos);


	Node();
	Node(int X, int Y);
private:
	int x;
	int y;
	int h;
	int g;
	int f;
	int type;

	bool processed;
	bool closed;

	Node* parent;
};

#endif