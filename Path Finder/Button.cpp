#include "Button.h"

Button::Button()
{
	x = 0;
	y = 0;
	w = 0;
	h = 0;
	clicked = false; 
	hover = false;
	texture = NULL;
}

Button::Button(int X, int Y, int W, int H)
{
	x = X;
	y = Y;
	w = W;
	h = H;
	clicked = false;
	hover = false;
	texture = NULL;
}

void Button::setX(int X)
{
	x = X;
}

void Button::setY(int Y)
{
	y = Y;
}

void Button::setW(int W)
{
	w = W;
}

void Button::setH(int H)
{
	h = H;
}

void Button::setClicked(bool Clicked)
{
	clicked = Clicked;
}

void Button::setHover(bool Hover)
{
	hover = Hover;
}

void Button::setTexture(SDL_Texture* Texture)
{
	texture = Texture;
}

void Button::setHighlightRing(SDL_Texture* HighlightRing)
{
	highlightRing = HighlightRing;
}

void Button::update()
{
	int mx, my;
	SDL_GetMouseState(&mx, &my);

	setHover(checkHover(mx, my));

	if (hover && (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)))//Checks to see if the button is being clicked.
	{
		setClicked(true);
	}
	else setClicked(false);
}

void Button::render(SDL_Renderer* renderer)
{
	SDL_Rect sRect = { 0, 0, w, h };
	SDL_Rect dRect = { x, y, w, h };
	SDL_Rect sRect1 = { 0, 0, w + 4, h + 4 };
	SDL_Rect dRect1 = { x - 2, y - 2, w + 4, h + 4 };

	SDL_RenderCopyEx(renderer, texture, &sRect, &dRect, 0, NULL, SDL_FLIP_NONE);
	if (hover) //Renders the highlight ring if the mouse is hovering over the button.
	{
		SDL_RenderCopyEx(renderer, highlightRing, &sRect1, &dRect1, 0, NULL, SDL_FLIP_NONE);
	}
}

bool Button::checkHover(int mx, int my)//Checks to see if the mouse is hovering over the button.
{
	if ((mx > x && mx < (x + w)) && (my > y && my < (y + h)))
	{
		return true;
	}
	return false;
}