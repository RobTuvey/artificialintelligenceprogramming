#include "Pathfinder.h"

Node Pathfinder::tileNodes[50][50];

Pathfinder::Pathfinder()
{
	targetFound = false;
}

Pathfinder::Pathfinder(Tile tiles[50][50])
{
	targetFound = false;

	for (int x = 0; x < 50; x++)
	{
		for (int y = 0; y < 50; y++)//Initializes the nodes and aligns them with the tiles.
		{
			tileNodes[x][y].setX(tiles[x][y].getTileX());
			tileNodes[x][y].setY(tiles[x][y].getTileY());
			tileNodes[x][y].setType(tiles[x][y].getType());
		}
	}
}

void Pathfinder::aStar(Node* startPos, Node* targetPos)
{
	clearLists();
	for (int i = 0; i < pathToGoal.size(); i++)//Makes sure the final list is clear to have the path put into it.
	{
		delete pathToGoal[i];
	}
	pathToGoal.clear();

	processNode(&tileNodes[startPos->getX()][startPos->getY()], targetPos);

	while (!targetFound && openList.size() > 0)
	{
		Node* nextNode = openList[getNextNode()];
		processNode(nextNode, targetPos);
	}

	plotPath(lastNode, startPos);
	clearLists();
}

void Pathfinder::processNode(Node* node, Node* targetPos)
//This processes the node, updating each node in all four directions of it
{
	int nodeX = node->getX(), nodeY = node->getY();
	Node* currentNode = &tileNodes[nodeX - 1][nodeY];	
	updateNode(currentNode, targetPos, node);
	currentNode = NULL;
	
	currentNode = &tileNodes[nodeX][nodeY + 1];
	updateNode(currentNode, targetPos, node);
	currentNode = NULL;

	currentNode = &tileNodes[nodeX + 1][nodeY];
	updateNode(currentNode, targetPos, node);
	currentNode = NULL;

	currentNode = &tileNodes[nodeX][nodeY - 1];
	updateNode(currentNode, targetPos, node);
	currentNode = NULL;

	if (!node->getClosed())//If the node isn't already on the closed list, this adds it to it.
	{
		closedList.push_back(node);
		node->setClosed(true);
	}
}

void Pathfinder::updateNode(Node* node, Node* targetPos, Node* parentNode)
{
	if (node->getX() == targetPos->getX() && node->getY() == targetPos->getY())//If the node is next to the target node it finishes the pathfinding.
	{
		targetFound = true;
		lastNode = parentNode;
	}
	else if (node->getType() > 1)//Checks to make sure it is not a wall or empty space.
	{
		if (node->getProcessed())//If the node is already on the open list, it just needs checking to see if this is a more efficient way to it.
		{
			if (node->getG() > (parentNode->getG() + 10))
			{
				node->setG(parentNode->getG() + 10);
				node->setParent(parentNode);
				node->setF(node->getG() + node->getH());
			}
		}
		else//Otherwise the node needs to be calculated and have it's parent set.
		{
			node->calcH(targetPos);
			node->setG(parentNode->getG() + 10);
			node->setF(node->getH() + node->getG());
			node->setProcessed(true);
			node->setParent(parentNode);
			openList.push_back(node);
		}
	}
}

void Pathfinder::plotPath(Node* node, Node* startPos)
//This pushes the path to the target into the path list, tracking back through the nodes using it's parent.
{
	if (node != NULL)
	{
		int nodeX = node->getX(), nodeY = node->getY();
		pathToGoal.push_back(new Vector2(nodeX, nodeY));
		if (!(nodeX == startPos->getX() && nodeY == startPos->getY()))
		{
			plotPath(node->getParent(), startPos);
		}
	}
}

void Pathfinder::clearLists()
//Clears both the open and closed list and empties all of the nodes.
{
	openList.clear();
	closedList.clear();
	for (int X = 0; X < 50; X++)
	{
		for (int Y = 0; Y < 50; Y++)
		{
			tileNodes[X][Y].setClosed(false);
			tileNodes[X][Y].setProcessed(false);
			tileNodes[X][Y].setH(0);
			tileNodes[X][Y].setG(0);
			tileNodes[X][Y].setF(0);
			tileNodes[X][Y].setParent(NULL);
		}
	}
}

bool Pathfinder::checkClosed(Node* node)
//Checks to see if a node is already closed.
{
	for (int i = 0; i < closedList.size(); i++)
	{
		if (closedList[i]->getX() == node->getX() && closedList[i]->getY() == node->getY());
		{
			return true;
		}
	}
	
	return false;
}

int Pathfinder::getNextNode()
//Find which node would be best to process next depending on its F score.
{
	int index = 0;

	int bestF = 0;

	for (int i = 0; i < openList.size(); i++)
	{
		if ((bestF == 0 || openList[i]->getF() < bestF) && !openList[i]->getClosed())
		{
			bestF = openList[i]->getF();
			index = i;
		}
	}

	return index;
}
