#include <SDL.h>

#ifndef CLOCK_H
#define CLOCK_H

class Clock
{
public:
	Clock();

	void start();
	void stop();
	void pause();
	void unpause();

	Uint32 getTicks();

	bool isStarted() { return started; }
	bool isPaused() { return paused; }
private:
	Uint32 startTicks;
	Uint32 pausedTicks;

	bool paused;
	bool started;
};

#endif