#include "Tile.h"

Tile::Tile()
{

}

void Tile::setX(int X)
{
	x = X;
}

void Tile::setY(int Y)
{
	y = Y;
}

void Tile::setW(int W)
{
	w = W;
}

void Tile::setH(int H)
{
	h = H;
}

void Tile::setTileX(int TileX)
{
	tilex = TileX;
}

void Tile::setTileY(int TileY)
{
	tiley = TileY;
}

void Tile::setType(int Type)
{
	type = Type;
}

void Tile::setVisited(bool Visited)
{
	visited = Visited;
}

void Tile::setMerged(bool Merged)
{
	merged = Merged;
}

void Tile::setTexture(SDL_Texture* Texture)
{
	texture = Texture;
}

void Tile::setHighlight(SDL_Texture* Highlight)
{
	highlight = Highlight;
}

void Tile::setPath(bool Path)
{
	path = Path;
}

bool Tile::checkHover(int mx, int my)
//Checks to see if the mouse is hovering over this specific tile.
{
	if ((mx >= x && mx < (x + w)) && (my >= y && my < (y + h)))
	{
		return true;
	}
	return false;
}

void Tile::update(int mx, int my)
{
	hover = (checkHover(mx, my));
}

void Tile::render(SDL_Renderer* renderer)
{
	SDL_Rect sRect = { 0, 0, w, h };
	SDL_Rect dRect = { x, y, w, h };

	SDL_RenderCopyEx(renderer, texture, &sRect, &dRect, 0, NULL, SDL_FLIP_NONE);

	if (hover) //if the mouse is hovering on the tile it renders the highlight ring around it.
	{
		SDL_RenderCopyEx(renderer, highlight, &sRect, &dRect, 0, NULL, SDL_FLIP_NONE);
	}
}