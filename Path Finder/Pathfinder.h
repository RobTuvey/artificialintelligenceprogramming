#ifndef PATHFINDER_H
#define PATHFINDER_H

#include "Node.h"
#include <vector>
#include "Vector.h"
#include "Tile.h"

class Pathfinder
{
public:
	void aStar(Node* startPos, Node* targetPos);
	void processNode(Node* node, Node* targetPos);
	void updateNode(Node* node, Node* targetPos, Node* parentNode);
	void plotPath(Node* node, Node* startPos);
	void clearLists();

	bool checkClosed(Node* node);

	std::vector<Vector2*> getPath(){ return pathToGoal; }

	int getNextNode();

	Pathfinder();
	Pathfinder(Tile tiles[50][50]);
	~Pathfinder();
private:
	std::vector<Node*> openList;
	std::vector<Node*> closedList;
	std::vector<Vector2*> pathToGoal;

	Node* lastNode;

	bool targetFound;

	static Node tileNodes[50][50];
};

#endif