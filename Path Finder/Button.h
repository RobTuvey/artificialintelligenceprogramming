#include "SDL.h"
#include "SDL_image.h"

#ifndef BUTTON_H
#define BUTTON_H

class Button
{
public:
	Button();
	Button(int X, int Y, int W, int H);

	int getX() { return x; }
	int getY() { return y; }
	int getW() { return w; }
	int getH() { return h; }
	bool getClicked() { return clicked; }
	bool getHover() { return hover; }
	SDL_Texture* getTexture() { return texture; }
	SDL_Texture* getHighlightRing() { return highlightRing; }

	void setX(int X);
	void setY(int Y);
	void setW(int W);
	void setH(int H);
	void setClicked(bool Clicked);
	void setHover(bool Hover);
	void setTexture(SDL_Texture* Texture);
	void setHighlightRing(SDL_Texture* HighlightTexture);

	void update();
	void render(SDL_Renderer* renderer);
private:
	int x;
	int y;
	int w;
	int h;
	bool clicked;
	bool hover;
	SDL_Texture* texture;
	SDL_Texture* highlightRing;

	bool checkHover(int x, int y);
};

#endif