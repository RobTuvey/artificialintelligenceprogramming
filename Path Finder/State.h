#ifndef STATE_H
#define STATE_H

#include "SDL.h"
#include "SDL_image.h"
#include <iostream>

class Context;

class State
{
public:
	State(Context* manager);
	virtual ~State();


	virtual void update(SDL_Renderer* renderer, bool &quit) = 0;
	
	State** self;

	SDL_Texture* loadTexture(SDL_Renderer* renderer, std::string filename);
protected:
	Context* manager;
};

#endif